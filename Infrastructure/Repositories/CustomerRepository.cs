﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using StoreMnagement.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.ApplicationServices;
using System.Web.ModelBinding;

namespace StoreMnagement.Infrastructure.Repositories
{
    public class CustomerRepository : ICustomerRepository, IDisposable
    {
        private IRoleRepository _roleRepository;
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;
        private ICartRepository _cartRepository;
        private ApplicationDbContext _db;
        public CustomerRepository(ApplicationDbContext db, IRoleRepository roleRepository, ICartRepository cartRepository, ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            _db = db;
            _roleRepository = roleRepository;
            _userManager = userManager;
            _signInManager = signInManager;
            _cartRepository = cartRepository;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool Login(LoginViewModel model)
        {
            if(model != null)
            {
                _cartRepository.MigrateShoppingCart(model.Email);
                return true;
            }
            return false;
        }

        public bool Register(RegisterViewModel customer)
        {
                if (customer != null)
                {
                    var user = new ApplicationUser { Email = customer.Email, UserName = customer.Email};
                    var result = _userManager.CreateAsync(user, customer.Password).Result;

                    if (result.Succeeded)
                    {
                        var customers = new Customer()
                        {
                            Email = user.Email,
                            FirstName = user.Email,
                            LastName = user.Email,
                            Address = user.Email
                        };

                        var role = _roleRepository.CreateRole("CUSTOMER");
                        if (role)
                        {
                            _userManager.AddToRole(user.Id, "CUSTOMER");
                        }
                        _db.Customers.Add(customers);
                        _db.SaveChanges();

                    /*_signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);*/
                    return true;
                    }
                    return false;
                }
            return false;
        }


        
    }
}