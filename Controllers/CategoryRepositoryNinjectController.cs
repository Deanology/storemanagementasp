﻿using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StoreMnagement.Controllers
{
    public class CategoryRepositoryNinjectController : Controller
    {
        ICategoryRepositoryNinject _iCategoryRepositoryNinject;
        public CategoryRepositoryNinjectController(ICategoryRepositoryNinject iCategoryRepositoryNinject)
        {
            _iCategoryRepositoryNinject = iCategoryRepositoryNinject;
        }
        // GET: CategoryRepositoryNinject
        public ActionResult Index()
        {
            var categoryList = _iCategoryRepositoryNinject.GetCategories();
            return View(categoryList);
        }
        [HttpGet]
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var category = _iCategoryRepositoryNinject.GetCategoryById(id.Value);
                return View(category);
            }
            catch(Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var category = _iCategoryRepositoryNinject.GetCategoryById(id.Value);
                return View(category);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,CategoryName,Description")] Category category)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                     ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                _iCategoryRepositoryNinject.UpdateCategory(category);
                return RedirectToAction("Index");

            }
            catch(Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var category = _iCategoryRepositoryNinject.GetCategoryById(id.Value);
                return View(category);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            /*Category category = _iCategoryRepositoryNinject.GetCategoryById(id.Value);*/
            _iCategoryRepositoryNinject.DeleteCategoryById(id);
            return RedirectToAction("Index");
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CategoryName,Description")] Category category)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Error = "Invalid Request sent, Try Again";
                return View();
            }
            _iCategoryRepositoryNinject.InsertCategory(category);
            return RedirectToAction("Index");
        }
    }
    
}