﻿using StoreMnagement.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace StoreMnagement.Infrastructure.Interfaces
{
    public interface ICartRepository : IDisposable
    {
        void AddToCart(int id);
        void RemoveFromCart(int id);
        void EmptyCart();
        List<Cart> GetCartItems();
        int GetCount();
        decimal GetTotal();
        int CreateOrder(Order order);
        void GetCart(HttpContext context);
        string GetCartId();
        void MigrateCart(string userName);
        void MigrateShoppingCart(string UserName);
        void UpdateItem(string updateCartID,int updateProductID, int quantity);
        void IncreaseQuantity(int id);
        void DecreaseQuantity(int id);
    }
}
