﻿using StoreMnagement.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreMnagement.Infrastructure.Interfaces
{
    public interface IOrderRepository : IDisposable
    {
        IEnumerable<Order> GetOrders();
        Order GetOrderById(string orderId);
        IEnumerable<Order> GetOrdersByUserId(string userId);
        void InsertOrder(Order order);
        void DeleteOrderById(string orderId);
        void UpdateOrder(Order order);
        void Save();
    }
}
