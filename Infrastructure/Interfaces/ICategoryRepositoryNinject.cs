﻿using StoreMnagement.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreMnagement.Infrastructure.Interfaces
{
    public interface ICategoryRepositoryNinject : IDisposable
    {
        IEnumerable<Category> GetCategories();
        Category GetCategoryById(int? categoryId);
        void InsertCategory(Category category);
        void DeleteCategoryById(int categoryId);
        void UpdateCategory(Category category);
        void Save();
    }
}
