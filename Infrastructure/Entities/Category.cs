﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.Entities
{
    public class Category
    {
        public int Id { get; set; }
        [Required, StringLength(100), Display(Name = "Category Name")]
        public string CategoryName { get; set; }
        [Display(Name = "Product Description")]
        public string Description { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}