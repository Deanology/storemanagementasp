﻿namespace StoreMnagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateOrder21 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Zip", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Zip");
        }
    }
}
