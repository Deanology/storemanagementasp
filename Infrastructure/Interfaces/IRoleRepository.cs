﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreMnagement.Infrastructure.Interfaces
{
    public interface IRoleRepository
    {
        bool CreateRole(string roleName);
    }
}
