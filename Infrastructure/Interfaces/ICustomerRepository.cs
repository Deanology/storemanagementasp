﻿using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreMnagement.Infrastructure.Interfaces
{
    public interface ICustomerRepository : IDisposable
    {
        bool Register(RegisterViewModel customer);
        bool Login(LoginViewModel model);
    }
}
