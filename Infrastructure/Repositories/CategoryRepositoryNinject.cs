﻿using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using StoreMnagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.Repositories
{
    public class CategoryRepositoryNinject : ICategoryRepositoryNinject, IDisposable
    {
        ApplicationDbContext _db;
        public CategoryRepositoryNinject(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteCategoryById(int categoryId)
        {
            Category category = GetCategoryById(categoryId);
            _db.Categories.Remove(category);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> GetCategories()
        {
            return _db.Categories.ToList();
        }

        public Category GetCategoryById(int? categoryId)
        {
            return _db.Categories.Find(categoryId.Value);
        }

        public void InsertCategory(Category category)
        {
            _db.Categories.Add(category);
            _db.SaveChanges();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void UpdateCategory(Category category)
        {
            _db.Entry(category).State = System.Data.Entity.EntityState.Modified;
            _db.SaveChanges();
        }
    }
}