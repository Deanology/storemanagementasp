﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.Entities
{
    public class Payment
    {
        public int PaymentId { get; set; }
        [Display(Name= "Card Name")]
        public string Cardname { get; set; }
        [Display(Name = "Card Number")]
        public string Cardnumber { get; set; }
        [Display(Name = "CVC")]
        public int Cvc { get; set; }
        [Display(Name = "Expiration Month")]
        public int Month { get; set; }
        [Display(Name = "Expiration Year")]
        public int Year { get; set; }
    }
}