﻿using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using StoreMnagement.Infrastructure.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StoreMnagement.Controllers
{
    [Authorize(Roles = "CUSTOMER")]
    public class CheckoutController : Controller
    {
        private ICartRepository _iCartRepository;
        private IOrderRepository _iOrderRepository;
        public CheckoutController(ICartRepository iCartRepository, IOrderRepository iOrderRepository)
        {
            _iCartRepository = iCartRepository;
            _iOrderRepository = iOrderRepository;
        }
        
        // GET: Checkout
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Checkout()
        {
            var viewModel = new CartViewModel
            {
                CartItems = _iCartRepository.GetCartItems(),
                CartTotal = _iCartRepository.GetTotal(),
                Count = _iCartRepository.GetCount(),
            };
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult Checkout(Order order)
        {
            if (ModelState.IsValid)
            {
                _iOrderRepository.InsertOrder(order);
                return RedirectToAction("Home", "Index");
            }
            return View("Checkout");
        }
        public ActionResult Payment()
        {
            return View();
        }
        public ActionResult Invoice()
        {
            return View();
        }
       
    }
}