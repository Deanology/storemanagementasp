﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.Entities
{
    public class Customer
    {
        public int Id { get; set; }
        /*[Required(ErrorMessage = "First Name is required")]
        [Display(Name="First Name")]
        [StringLength(160)]*/
        public string FirstName { get; set; }

        /*[Required(ErrorMessage = "Last Name is required")]
        [Display(Name ="Last Name")]
        [StringLength(160)]*/
        public string LastName { get; set; }


       /* [Display(Name = "User Name")]
        [StringLength(160)]*/
        public string UserName { get; set; }

        /*[Required(ErrorMessage = "Address is required")]
        [StringLength(200, MinimumLength =10)]*/
        public string Address { get; set; }

       /* [Required(ErrorMessage = "Email is required")]
        [EmailAddress]*/
        [Display(Name = "Email")]
        public string Email { get; set; }

      /*  [Required(ErrorMessage = "Phone is required")]
        [StringLength(24)]
        [Display(Name = "Phone Number")]*/
        public string Phone { get; set; }

       /* [Required(ErrorMessage = "City is required")]
        [StringLength(40)]*/
        public string City { get; set; }

       /* [Required(ErrorMessage = "State is required")]
        [Display(Name ="State/Region")]
        [StringLength(40)]*/
        public string State { get; set; }


       /* [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]*/
        public string Password { get; set; }

       /* [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]*/
        public string ConfirmPassword { get; set; }
    }
}