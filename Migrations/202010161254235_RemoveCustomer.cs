﻿namespace StoreMnagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveCustomer : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "FirstName", c => c.String());
            AlterColumn("dbo.Customers", "LastName", c => c.String());
            AlterColumn("dbo.Customers", "UserName", c => c.String());
            AlterColumn("dbo.Customers", "Address", c => c.String());
            AlterColumn("dbo.Customers", "Email", c => c.String());
            AlterColumn("dbo.Customers", "Phone", c => c.String());
            AlterColumn("dbo.Customers", "City", c => c.String());
            AlterColumn("dbo.Customers", "State", c => c.String());
            AlterColumn("dbo.Customers", "Password", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "Password", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Customers", "State", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.Customers", "City", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.Customers", "Phone", c => c.String(nullable: false, maxLength: 24));
            AlterColumn("dbo.Customers", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Customers", "Address", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Customers", "UserName", c => c.String(maxLength: 160));
            AlterColumn("dbo.Customers", "LastName", c => c.String(nullable: false, maxLength: 160));
            AlterColumn("dbo.Customers", "FirstName", c => c.String(nullable: false, maxLength: 160));
        }
    }
}
