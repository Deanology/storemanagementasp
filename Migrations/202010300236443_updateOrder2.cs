﻿namespace StoreMnagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateOrder2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ShippingDetails", c => c.Boolean());
            AddColumn("dbo.Orders", "Save", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Save");
            DropColumn("dbo.Orders", "ShippingDetails");
        }
    }
}
