﻿using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using StoreMnagement.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.Repositories
{
    public class ProductRepositoryNinject : IProductRepositoryNinject, IDisposable
    {
        ApplicationDbContext _db;
        public ProductRepositoryNinject(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteProductById(int productId)
        {
            Product product = GetProductById(productId);
            _db.Products.Remove(product);
            _db.SaveChanges();
        }

        public List<Product> DisplayProduct(string productCategory, string searchString)
        {
            var product = from m in _db.Products
                          select m;
            if (!String.IsNullOrEmpty(searchString))
            {
                product = product.Where(s => s.ProductName.Contains(searchString) || s.Category.CategoryName.Equals(searchString));
            }
            return product.ToList();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Product GetProductById(int? productId)
        {
            return _db.Products.Find(productId.Value);
        }

        public IEnumerable<Product> GetProducts()
        {
            return _db.Products.Include(p => p.Category);
        }

        public void InsertProduct(Product product)
        {
            _db.Products.Add(product);
            _db.SaveChanges();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void UpdateProduct(Product product)
        {
            _db.Entry(product).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public DbSet<Category> Any()
        {
            return _db.Categories;
        }
    }
}