﻿using StoreMnagement.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.Interfaces
{
    public interface IProductRepositoryNinject : IDisposable
    {
        IEnumerable<Product> GetProducts();
        Product GetProductById(int? productId);
        void InsertProduct(Product product);
        void DeleteProductById(int productId);
        void UpdateProduct(Product product);
        void Save();
        List<Product> DisplayProduct(string productCategory, string searchString);
        DbSet<Category> Any();
    }
}