﻿using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using StoreMnagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace StoreMnagement.Infrastructure.Repositories
{
    public class StockRepository : IStockRepository
    {
        private ApplicationDbContext _db;
        public StockRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void CreateStock(Stock stock)
        {
            _db.Stocks.Add(stock);
            _db.SaveChanges();
        }

        public async Task<bool> DeleteStockById(int stockId)
        {
            var stock = GetStockById(stockId);
            _db.Stocks.Remove(stock);
            await _db.SaveChangesAsync();

            return true;
        }

        public IEnumerable<Stock> GetStock()
        {
            return _db.Stocks.ToList();
        }

        public Stock GetStockById(int? stockId)
        {
            return _db.Stocks.Find(stockId.Value);
        }

        public async Task<bool> UpdateCategory(Stock stock)
        {
            _db.Entry(stock).State = System.Data.Entity.EntityState.Modified;
            await _db.SaveChangesAsync();

            return true;
        }

    }
}