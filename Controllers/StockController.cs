﻿using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StoreMnagement.Controllers
{
    public class StockController : Controller
    {
        IStockRepository _iStockRepository;
        public StockController(IStockRepository iStockRepository)
        {
            _iStockRepository = iStockRepository;
        }
        // GET: Stock
        [HttpGet]
        public ActionResult Index()
        {
            var stocks = _iStockRepository.GetStock();
            return View(stocks);
        }
        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var stock = _iStockRepository.GetStockById(id.Value);
            if (stock == null)
            {
                return HttpNotFound();
            }
            return View(stock);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Stock stock)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Error = "Invalid Request sent, Try Again";
                return View();
            }
            _iStockRepository.CreateStock(stock);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var stock = _iStockRepository.GetStockById(id.Value);
                return View(stock);

            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpPost]
        public ActionResult Edit(Stock stock)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                _iStockRepository.UpdateCategory(stock);
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var stock = _iStockRepository.GetStockById(id.Value);
                return View(stock);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _iStockRepository.DeleteStockById(id);
            return RedirectToAction("Index");
        }
    }
}