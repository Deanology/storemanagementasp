﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.Entities
{
    public class Product
    {
        public int Id { get; set; }
        [Required, StringLength(100), Display(Name = "Name")]
        public string ProductName { get; set; }
        [Required, StringLength(1000), Display(Name = "Product Description"), DataType(DataType.MultilineText)]
        public string ProductDescription { get; set; }
        [Required, Display(Name = "Product Image")]
        public string ProductImagePath { get; set; }
        [Required, Display(Name = "Price")]
        public decimal ProductUnitPrice { get; set; }
        public int Quantity { get; set; }
        public DateTime DateAdded { get; set; } = DateTime.Now;
        public DateTime ExpiryDate { get; set; }
        
        public int? CategoryID { get; set; }
        public virtual Category Category { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}