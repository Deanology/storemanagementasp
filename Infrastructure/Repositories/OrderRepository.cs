﻿using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using StoreMnagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.Repositories
{
    public class OrderRepository : IOrderRepository, IDisposable
    {
        private ApplicationDbContext _db;
        public OrderRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteOrderById(string orderId)
        {
            Order order = _db.Orders.Find(orderId);
            _db.Orders.Remove(order);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Order GetOrderById(string orderId)
        {
            return _db.Orders.Find(orderId);
        }

        public IEnumerable<Order> GetOrders()
        {
            return _db.Orders;
        }

        public IEnumerable<Order> GetOrdersByUserId(string userId)
        {
            //still doesnt show a particular user owns the order
            /*return db.Orders.Where(a => a.Username == userId);*/
            throw new NotImplementedException();
        }

        public void InsertOrder(Order order)
        {
            _db.Orders.Add(order);
            _db.SaveChanges();
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public void UpdateOrder(Order order)
        {
            throw new NotImplementedException();
        }
    }
}