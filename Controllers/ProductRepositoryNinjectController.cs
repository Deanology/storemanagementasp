﻿using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace StoreMnagement.Controllers
{
    public class ProductRepositoryNinjectController : Controller
    {
        IProductRepositoryNinject _iProductRepositoryNinject;
        public ProductRepositoryNinjectController(IProductRepositoryNinject iProductRepositoryNinject)
        {
            _iProductRepositoryNinject = iProductRepositoryNinject;
        }
        // GET: ProductRepositoryNinject
        public ActionResult Index()
        {
            var productList = _iProductRepositoryNinject.GetProducts();
            return View(productList);
        }
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(_iProductRepositoryNinject.Any(), "Id", "CategoryName");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProductName,ProductDescription,ProductImagePath,ProductUnitPrice,Quantity,ExpiryDate,CategoryID")] Product product)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.CategoryID = new SelectList(_iProductRepositoryNinject.Any(), "Id", "CategoryName", product.CategoryID);
                return View(product);
            }
            _iProductRepositoryNinject.InsertProduct(product);
            return RedirectToAction("Index");
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var product = _iProductRepositoryNinject.GetProductById(id.Value);
                return View(product);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }
                var product = _iProductRepositoryNinject.GetProductById(id.Value);
                return View(product);
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"An Error Occured: {ex.Message.ToString()}";
            }
            return View();
        }
        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _iProductRepositoryNinject.DeleteProductById(id);
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            if(id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Product product = _iProductRepositoryNinject.GetProductById(id.Value);

            if (product == null)
                return HttpNotFound();

            ViewBag.CategoryID = new SelectList(_iProductRepositoryNinject.Any(), "Id","CategoryName", product.CategoryID);
            return View(product);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ProductName,ProductDescription,ProductImagePath,ProductUnitPrice,Quantity,DateAdded,ExpiryDate,CategoryID")] Product product)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.CategoryID = new SelectList(_iProductRepositoryNinject.Any(), "Id", "CategoryName", product.CategoryID);
                return View(product);
            }
            _iProductRepositoryNinject.UpdateProduct(product);
            return RedirectToAction("Index");
        }
    }
}