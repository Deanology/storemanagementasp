﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StoreMnagement.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace StoreMnagement.Infrastructure.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        private RoleManager<IdentityRole> _roleManager;
        public RoleRepository()
        {

        }
        public RoleRepository(RoleManager<IdentityRole> roleManager)
        {
            this._roleManager = roleManager;
        }
        public RoleManager<IdentityRole> RoleManager
        {
            get
            {
                return _roleManager ?? new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
            }
        }
        public bool CreateRole(string roleName)
        {
            var role = new IdentityRole(roleName);
            if (RoleManager.RoleExists(roleName))
            {
                return true;
            }
            var result = RoleManager.Create(role);
            return result.Succeeded;
        }
    }
}