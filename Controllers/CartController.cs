﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using StoreMnagement.Infrastructure.Repositories;
using StoreMnagement.Infrastructure.ViewModel;
using StoreMnagement.Models;

namespace StoreMnagement.Controllers
{
    public class CartController : Controller
    {
        
        private ICartRepository _cartRepository;
        public CartController(ICartRepository cartRepository)
        {
            _cartRepository = cartRepository;
        }
       
        public ActionResult AddToCart(int id)
        {
            _cartRepository.AddToCart(id);
            return RedirectToAction("Index", "Home");
        }
        public ActionResult DisplayCart()
        {
            var viewModel = new CartViewModel
            {
                CartItems = _cartRepository.GetCartItems(),
                CartTotal = _cartRepository.GetTotal(),
                Count = _cartRepository.GetCount()
            };
            return View(viewModel);
        }
        [HttpGet]
        public ActionResult RemoveFromCart(int id)
        {
            _cartRepository.RemoveFromCart(id);
            return RedirectToAction("DisplayCart", "Cart");
        }
        [HttpGet]
        public ActionResult IncreaseQuantity(int id)
        {
            _cartRepository.IncreaseQuantity(id);
            return RedirectToAction("DisplayCart", "Cart");
        }
        [HttpGet]
        public ActionResult DecreaseQuantity(int id)
        {
            _cartRepository.DecreaseQuantity(id);
            return RedirectToAction("DisplayCart", "Cart");
        }
         public ActionResult CartSummary()
        {
            ViewData["CartCount"] = _cartRepository.GetCount();
            return PartialView("CartSummary");
        }
    }
}
