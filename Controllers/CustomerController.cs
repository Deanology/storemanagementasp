﻿
using Microsoft.AspNet.Identity.Owin;
using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using StoreMnagement.Infrastructure.Repositories;
using StoreMnagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace StoreMnagement.Controllers
{
    public class CustomerController : Controller
    {
        private ICustomerRepository _iCustomerRepository;
        private ApplicationUserManager _applicationUserManager;
        private ApplicationSignInManager _applicationSignInManager;
        public CustomerController(ICustomerRepository iCustomerRepository, ApplicationUserManager applicationUserManager, ApplicationSignInManager applicationSignInManager)
        {
            _iCustomerRepository = iCustomerRepository;
            _applicationUserManager = applicationUserManager;
            _applicationSignInManager = applicationSignInManager;
        }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var boolResult = _iCustomerRepository.Login(model);
            if (!boolResult)
            {
                return RedirectToAction("Login", "Customer");
            }
            var result = await _applicationSignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }
            public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel customer)
         {
             if (ModelState.IsValid)
            {
                var result =  _iCustomerRepository.Register(customer);
                if (result)
                {
                    return RedirectToAction("Login", "Customer");
                }
                else
                {
                    return View(customer);
                }
            }
            return View();
        }
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Checkout", "Checkout");
        }
    }
}