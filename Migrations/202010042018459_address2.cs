namespace StoreMnagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class address2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Address2", c => c.String(maxLength: 70));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Address2");
        }
    }
}
