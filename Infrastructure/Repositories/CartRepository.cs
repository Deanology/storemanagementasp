﻿using StoreMnagement.Infrastructure.Entities;
using StoreMnagement.Infrastructure.Interfaces;
using StoreMnagement.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.Repositories
{
    public class CartRepository : ICartRepository, IDisposable
    {
        public string ShoppingCartId { get; set; }
        
        private ApplicationDbContext db;
        public CartRepository(ApplicationDbContext db)
        {
            this.db = db;
        }
        public const string CartSessionKey = "CartId";
        public void AddToCart(int id)
        {
            //retrieve the product from the database
            ShoppingCartId = GetCartId();

            var cart = db.Carts.SingleOrDefault(
                    c => c.Id == ShoppingCartId && c.ProductId == id
                );

            //create a new cart item if no cart item exists
            if (cart == null)
            {
                //Create a new cart item if no cart item exists
                cart = new Cart
                {
                    ProductId = id,
                    Quantity = 1,
                    DateCreated = DateTime.Now,
                    Product = db.Products.SingleOrDefault(p => p.Id == id),
                    Id = ShoppingCartId
                    
                };
                db.Carts.Add(cart);
            }
            else
            {
                //if the item does not exist in the cart, then add one to the quantity
                cart.Quantity++;
            }
            db.SaveChanges();
        }

        public int CreateOrder(Order order)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            if (db != null)
            {
                db.Dispose();
                db = null;
            }
        }

        public void EmptyCart()
        {
            ShoppingCartId = GetCartId();
            var cart = db.Carts.Where(c => c.Id == ShoppingCartId);
            foreach (var item in cart)
            {
                db.Carts.Remove(item);
            }
            //save changes 
            db.SaveChanges();
        }

        public void GetCart(HttpContext context)
        {
            throw new NotImplementedException();
        }

        public string GetCartId()
        {
            if (HttpContext.Current.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
                {
                    HttpContext.Current.Session[CartSessionKey] = HttpContext.Current.User.Identity.Name;
                }
                else
                {
                    //generate a new random GUID usingSystem.Guid class
                    Guid tempCartId = Guid.NewGuid();
                    HttpContext.Current.Session[CartSessionKey] = tempCartId.ToString();
                }
            }
            return HttpContext.Current.Session[CartSessionKey].ToString();
        }

        public List<Cart> GetCartItems()
        {
            ShoppingCartId = GetCartId();
            return db.Carts.Where(c => c.Id == ShoppingCartId).ToList();
        }

        public int GetCount()
        {
            ShoppingCartId = GetCartId();
            //get the count of each item in the cart
            int? count = (from cart in db.Carts
                          where cart.Id == ShoppingCartId
                          select (int?)cart.ItemId).Count();
            //Return 0 if all entries are null
            return count ?? 0;
        }

        public decimal GetTotal()
        {
            ShoppingCartId = GetCartId();

            decimal? total = decimal.Zero;
            total = (decimal?)(from item in db.Carts
                               where item.Id == ShoppingCartId
                               select (int?)item.Quantity *
                               item.Product.ProductUnitPrice).Sum();
            return total ?? decimal.Zero;
        }
        //when a user has logged in, migrate their shopping cart to be associated with their username
        public void MigrateCart(string userName)
        {
            ShoppingCartId = GetCartId();
            var cart = db.Carts.Where(c => c.Id == ShoppingCartId);
            foreach (var item in cart)
            {
                item.Id = userName;
            }
            db.SaveChanges();
        }
        public void MigrateShoppingCart(string userName)
        {
            //associate shopping cart items with logged-in user
            MigrateCart(userName);
            var Session = HttpContext.Current.Session;
            Session[CartSessionKey] = userName;
        }
        public void RemoveFromCart(int id)
        {
            //retrieve the product from the database
            ShoppingCartId = GetCartId();
            try
            {

                var cart = db.Carts.Single(c => c.Id == ShoppingCartId
                && c.ItemId == id);
               

                if(cart != null)
                {
                    db.Carts.Remove(cart);

                    db.SaveChanges();
                }
                /*return itemCount;*/
            }
            catch (Exception exp)
            {
                throw new Exception("ERROR: Unable to Remove Cart Item - " + exp.Message.ToString(), exp);
            }


        }

        public void UpdateItem(string updateCartID, int updateProductID, int quantity)
        {
            try
            {
                var item = (from c in db.Carts
                            where c.Id == updateCartID
                            && c.ProductId == updateProductID
                            select c).FirstOrDefault();
                if(item != null)
                {
                    item.Quantity = quantity;
                    db.SaveChanges();
                }
            }
            catch (Exception exp)
            {
                throw new Exception("ERROR: Unable to Update Cart Item - " + exp.Message.ToString(), exp);

            }
        }

        public void IncreaseQuantity(int id)
        {
            //retrieve the product from the database
            ShoppingCartId = GetCartId();
            try
            {
                var cart = db.Carts.FirstOrDefault(c => c.Id == ShoppingCartId && c.ItemId == id);
                if(cart != null)
                {
                    cart.Quantity++;
                    db.Entry(cart).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    throw new Exception("Product not found");
                }
            }
            catch(Exception exp)
            {
                throw new Exception("ERROR: Unable to Increase Quantity - " + exp.Message.ToString(), exp);
            }

        }

        public void DecreaseQuantity(int id)
        {
            //retrieve the product from the database
            ShoppingCartId = GetCartId();
            try
            {
                var cart = db.Carts.FirstOrDefault(c => c.Id == ShoppingCartId && c.ItemId == id);
                if (cart != null)
                {
                    cart.Quantity--;
                    db.Entry(cart).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    throw new Exception("Product not found");
                }
            }
            catch (Exception exp)
            {
                throw new Exception("ERROR: Unable to Increase Quantity - " + exp.Message.ToString(), exp);
            }
        }

    }
}