﻿namespace StoreMnagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateOrder1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Orders", "Address2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "Address2", c => c.String(maxLength: 70));
        }
    }
}
