﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.Entities
{
    public class Cart
    {
            [Key]
            public int ItemId { get; set; }
            public string Id { get; set; }
            public int Quantity { get; set; }
            public System.DateTime DateCreated { get; set; }
            public int ProductId { get; set; }
            public virtual Product Product { get; set; }
    }
}