﻿using StoreMnagement.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StoreMnagement.Controllers
{
    public class HomeController : Controller
    {
        IProductRepositoryNinject _iProductRepositoryNinject;
        public HomeController(IProductRepositoryNinject iProductRepositoryNinject)
        {
            _iProductRepositoryNinject = iProductRepositoryNinject;
        }
        // GET: HomeRepositoryNinject
        public ActionResult Index(string productCategory, string searchString)
        {
            var product = _iProductRepositoryNinject.DisplayProduct(productCategory, searchString);
            return View(product);
        }
        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var product = _iProductRepositoryNinject.GetProductById(id.Value);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }
    }
}