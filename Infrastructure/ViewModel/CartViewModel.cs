﻿using StoreMnagement.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoreMnagement.Infrastructure.ViewModel
{
    public class CartViewModel
    {
        public List<Cart> CartItems { get; set; }
        public decimal CartTotal { get; set; }
        public int Count { get; set; }
        public Order Orders { get; set; }
    }
}