﻿using StoreMnagement.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreMnagement.Infrastructure.Interfaces
{
    public interface IStockRepository
    {
        void CreateStock(Stock stock);
        IEnumerable<Stock> GetStock();
        Stock GetStockById(int? stockId);
        Task<bool> DeleteStockById(int stockyId);
        Task<bool> UpdateCategory(Stock stock);
    }
}
