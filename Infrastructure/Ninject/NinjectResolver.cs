﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using System.Web.Http.Dependencies;
using StoreMnagement.Infrastructure.Interfaces;
using StoreMnagement.Infrastructure.Repositories;
using StoreMnagement.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Ninject.Web.Common;
using Microsoft.AspNet.Identity.Owin;

namespace StoreMnagement.Infrastructure.Ninject
{
    public class NinjectResolver : IDependencyResolver, System.Web.Mvc.IDependencyResolver
    {
        private IKernel _ninjectKernel;
        public NinjectResolver(IKernel ninjectKernel)
        {
            _ninjectKernel = ninjectKernel;
            AddBindings();
        }
        private void AddBindings()
        {
            _ninjectKernel.Bind<IProductRepositoryNinject>().To<ProductRepositoryNinject>();
            _ninjectKernel.Bind<ICategoryRepositoryNinject>().To<CategoryRepositoryNinject>();
            _ninjectKernel.Bind<ICartRepository>().To<CartRepository>();
            _ninjectKernel.Bind<ICustomerRepository>().To<CustomerRepository>();
            _ninjectKernel.Bind<ICheckoutRepository>().To<CheckoutRepository>();
            _ninjectKernel.Bind<IRoleRepository>().To<RoleRepository>();
            _ninjectKernel.Bind<IOrderRepository>().To<OrderRepository>();
            _ninjectKernel.Bind<ApplicationDbContext>().ToSelf();

            _ninjectKernel.Bind<ApplicationUserManager>().ToMethod(c => HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>()).InRequestScope();
            _ninjectKernel.Bind<ApplicationSignInManager>().ToSelf();
          
            _ninjectKernel.Bind<IRoleStore<IdentityRole, string>>().To<RoleStore<IdentityRole, string, IdentityUserRole>>();
            _ninjectKernel.Bind<RoleManager<IdentityRole, string>>().ToSelf();
            _ninjectKernel.Bind<IUserStore<ApplicationUser>>().To<UserStore<ApplicationUser>>();
            _ninjectKernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication).InRequestScope();

        }
        public IDependencyScope BeginScope()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public object GetService(Type serviceType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> GetServices(Type serviceType) => _ninjectKernel.GetAll(serviceType);

    }
}